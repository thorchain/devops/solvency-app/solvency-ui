import { 
	useEffect,
	useMemo,
	useState,
} from 'react'

import './App.css'
import Table from './Table'
import { fetchLatestSnapShot } from './behaviours'
import { fetchLatestSnapShotPresenter } from './presenters'

function App() {
	const columns = useMemo(
		() => [
			{
				Header: 'THORChain Solvency Data',
				columns: [
					{
						Header: 'Asset',
						accessor: 'symbol'
					},
					{
						Header: 'Pool Balance',
						accessor: 'poolBalance'
					},
					{
						Header: 'Vault Balance',
						accessor: 'vaultBalance'
					},
					{
						Header: 'Wallet Balance',
						accessor: 'walletBalance'
					},
					{
						Header: 'Diff (Pool vs. Vault)',
						accessor: 'poolVsVaultDiff'
					},
					{
						Header: 'Diff (Vault vs. Wallet)',
						accessor: 'walletVsVaultDiff'
					},
					{
						Header: 'Status',
						accessor: 'statusOK'
					},
				]
			},
		],
		[]
	)

	const [data, setData] = useState([])
	useEffect( async () => {
		fetchLatestSnapShot({ presenter: fetchLatestSnapShotPresenter, callback: setData })
	}, [])
    
	return (
		<div>
			<div className="container-fluid page-content">
				<div className="table-wrapper">
					<Table data={ data } columns={ columns } />
				</div>
			</div>
		</div>
	)
}

export default App
